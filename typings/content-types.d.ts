interface Asset {
  mimeType: string;
  url: string;
  handle: string;
}

interface NeighborCountry {
  id: string;
  name: string;
  flag: Asset;
}

interface Currency {
  code: string;
  name: string;
  symbol?: string;
}

interface Language {
  code: string;
  name: string;
}

interface Location {
  latitude: number;
  longitude: number;
}

interface Region {
  name: string;
}
