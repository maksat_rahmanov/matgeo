interface DetailItem {
  title?: string;
  subtitle: string;
  icon: string;
  items?: string[];
}

interface MapSettings {
  lat: number;
  lon: number;
  zoom: number;
  minZoom: number;
  maxZoom: number;
}

interface AbstractModel {
  id: string;
}

interface ConnectionQuery<T = {}> {
  edges: {
    node: T;
  }[];
  pageInfo: {
    hasNextPage: boolean;
    endCursor: string;
  };
}

type PageInfo = ConnectionQuery["pageInfo"];
