import { gql } from "apollo-angular";

const edge = gql`
  fragment countryNodeFragment on CountryEdge {
    node {
      id
      name
      capital
      flag {
        url
      }
    }
  }

  fragment pageInfoFragment on PageInfo {
    hasNextPage
    endCursor
  }
`;

const countriesQueryFragment = `
  pageInfo {
    ...pageInfoFragment
  }
  edges {
    ...countryNodeFragment
  }
`;

export const getCountriesQuery = gql`
  query {
    countriesConnection(orderBy: name_ASC, first: 100) {
      ${countriesQueryFragment}
    }
  }
  ${edge}
`;

export const getNextCountriesQuery = gql`
  query ($cursor: String, $size: Int) {
    countriesConnection(orderBy: name_ASC, first: $size, after: $cursor) {
      ${countriesQueryFragment}
    }
  }
  ${edge}
`;

export const searchCountryQuery = gql`
  query ($searchQuery: String) {
    countriesConnection(
      where: {
        OR: [
          { name_contains: $searchQuery }
          { capital_contains: $searchQuery }
        ]
      }
    ) {
      ${countriesQueryFragment}
    }
  }
  ${edge}
`;

export const getCountryDetailQuery = gql`
  query ($id: ID) {
    country(where: { id: $id }) {
      name
      capital
      area
      flag {
        url
      }
      heroImage {
        mimeType
        url
        handle
      }
      region {
        name
      }
      subregion {
        name
      }
      population
      location {
        latitude
        longitude
      }
      alpha3Code
      alpha2Code
      currencies {
        code
        name
        symbol
      }
      timeZones
      topLevelDomain
      callingCodes
      languages {
        code
        name
      }
      borderCountries {
        id
        name
        flag {
          url
        }
      }
    }
  }
`;
