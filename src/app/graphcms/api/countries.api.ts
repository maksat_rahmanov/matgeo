import { Injectable } from "@angular/core";
import { Apollo } from "apollo-angular";
import { filter, Observable } from "rxjs";
import { map } from "rxjs/operators";
import { CountryContentType } from "../../content-types/country.content-type";
import { Country } from "../../model/country";
import {
  getCountriesQuery,
  getCountryDetailQuery,
  getNextCountriesQuery,
  searchCountryQuery
} from "../queries/query";
import { ApolloQueryResult, QueryOptions } from "@apollo/client/core";
import { Constants } from "../../app.constants";

interface CountriesConnectionQuery {
  countriesConnection: ConnectionQuery<CountryContentType>;
}

interface CountryDetailsQuery {
  country: CountryContentType;
}

export interface CountriesConnection {
  pageInfo: PageInfo;
  countries: Country[];
}

@Injectable({
  providedIn: "root"
})
export class CountriesApiService {
  constructor(private apollo: Apollo) {}

  getCountries(): Observable<CountriesConnection> {
    return this.queryCountriesConnection({
      query: getCountriesQuery
    });
  }

  getNextCountries(cursor: string): Observable<CountriesConnection> {
    return this.queryCountriesConnection({
      query: getNextCountriesQuery,
      variables: {
        cursor,
        size: Constants.listPageSize
      }
    });
  }

  getCountriesBySearchString(
    searchString: string
  ): Observable<CountriesConnection> {
    return this.queryCountriesConnection({
      query: searchCountryQuery,
      variables: {
        searchQuery: searchString
      }
    });
  }

  getCountryById(id: string): Observable<Country> {
    const response = this.executeQuery<CountryDetailsQuery>({
      query: getCountryDetailQuery,
      variables: { id }
    });

    return response.pipe(
      map(({ data }) => new CountryContentType(data.country).mapToModel())
    );
  }

  private queryCountriesConnection(
    options: QueryOptions
  ): Observable<CountriesConnection> {
    const response = this.executeQuery<CountriesConnectionQuery>(options);
    return this.handleResponse(response);
  }

  private executeQuery<T>(
    options: QueryOptions
  ): Observable<ApolloQueryResult<T>> {
    return this.apollo.query<T>(options);
  }

  private handleResponse(
    observable: Observable<ApolloQueryResult<CountriesConnectionQuery>>
  ): Observable<CountriesConnection> {
    return observable.pipe(
      filter((result) => !!result.data),
      map((result) => result.data.countriesConnection),
      map(({ edges, pageInfo }) => ({
        pageInfo,
        countries: edges.map(({ node }) =>
          new CountryContentType(node).mapToModel()
        )
      }))
    );
  }
}
