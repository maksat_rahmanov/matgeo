import { LOCALE_ID, NgModule } from "@angular/core";
import { APOLLO_OPTIONS, ApolloModule } from "apollo-angular";
import { HttpLink } from "apollo-angular/http";
import { ApolloClientOptions, InMemoryCache } from "@apollo/client/core";
import { environment } from "../../environments/environment";
import { CountriesApiService } from "./api/countries.api";
import { HttpHeaders } from "@angular/common/http";

const defaultLanguage = "en";

function createApollo(
  httpLink: HttpLink,
  locale: string
): ApolloClientOptions<any> {
  const locales =
    locale.toLowerCase() === defaultLanguage
      ? [locale]
      : [locale, defaultLanguage];

  return {
    cache: new InMemoryCache(),
    link: httpLink.create({
      uri: environment.graphCmsEndpoint,
      headers: new HttpHeaders({
        "gcms-locales": locales
      })
    })
  };
}

@NgModule({
  exports: [ApolloModule],
  providers: [
    CountriesApiService,
    {
      provide: APOLLO_OPTIONS,
      useFactory: createApollo,
      deps: [HttpLink, LOCALE_ID]
    }
  ]
})
export class GraphcmsModule {}
