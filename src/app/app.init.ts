import { Store } from '@ngrx/store';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ActionTypes } from './store/action.types';
import { selectError, selectAllCountries } from '@store/selectors';
import { LANGUAGES, THEMES } from './app.settings';
import { Constants } from './app.constants';

export function initApp(store: Store, doc: Document) {
  return () => {
    return new Promise<void>((resolve, reject) => {
      const theme = getTheme(doc);
      store.dispatch(ActionTypes.setTheme(theme));

      const loaded$ = new Subject<void>();
      store.dispatch(ActionTypes.loadCountries());

      store
        .select(selectError)
        .pipe(takeUntil(loaded$))
        .subscribe((hasError) => {
          if (hasError) {
            loaded$.next();
            reject('Could not load data. Please refresh page');
          }
        });

      store
        .select(selectAllCountries)
        .pipe(takeUntil(loaded$))
        .subscribe((countries) => {
          if (countries.length) {
            loaded$.next();
            resolve();
          }
        });
    });
  };
}

export function getLanguage(doc: Document): string {
  const languageCodes = Array.from(LANGUAGES.keys());
  // EN language by default
  return (
    getValueFromCookie(
      doc,
      new RegExp(`${Constants.languageCookie}=([^;]+)`)
    ) || languageCodes[0]
  );
}

function getTheme(doc: Document) {
  const themeCodes = Array.from(THEMES.keys());
  // Dark theme by default
  return (
    getValueFromCookie(doc, new RegExp(`${Constants.themeCookie}=([^;]+)`)) ||
    themeCodes[0]
  );
}

function getValueFromCookie(doc: Document, regexp: RegExp): string {
  const match = doc.cookie.match(regexp);
  return match ? match[1] : '';
}
