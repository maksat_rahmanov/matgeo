import { BreakpointObserver } from "@angular/cdk/layout";
import {
  AfterViewInit,
  Component,
  ElementRef,
  OnDestroy,
  OnInit,
  ViewChild
} from "@angular/core";
import { Router } from "@angular/router";
import { fromEvent, Observable, Subject } from "rxjs";
import {
  debounceTime,
  distinctUntilChanged,
  map,
  takeUntil
} from "rxjs/operators";
import { Country } from "../../model/country";
import { MatSnackBar } from "@angular/material/snack-bar";
import { TranslocoService } from "@ngneat/transloco";
import { Store } from "@ngrx/store";
import { selectCountry, selectError } from "@store/selectors";
import { ActionTypes } from "@store/action.types";
import { Constants } from "../../app.constants";

@Component({
  selector: "matgeo-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.scss"]
})
export class HomeComponent implements OnInit, OnDestroy, AfterViewInit {
  country$!: Observable<Country | undefined>;
  isSearchVisible = false;
  isMobileView = false;

  @ViewChild("searchField") searchField!: ElementRef;

  private _destroy$ = new Subject<void>();

  constructor(
    private _breakpointObserver: BreakpointObserver,
    private _router: Router,
    private _snackBar: MatSnackBar,
    private _transloco: TranslocoService,
    private _store: Store
  ) {}

  ngOnInit() {
    this._store.dispatch(ActionTypes.setAppTitle(Constants.appTitle));
    this.country$ = this._store.select(selectCountry);

    this._store
      .select(selectError)
      .pipe(takeUntil(this._destroy$))
      .subscribe((isError) => isError && this.handleError());

    this._breakpointObserver
      .observe("(min-width: 768px)")
      .pipe(
        takeUntil(this._destroy$),
        map((breakpoint) => breakpoint.matches)
      )
      .subscribe((isMatched) => {
        this._store.dispatch(ActionTypes.setCountryId(""));
        this._store.dispatch(ActionTypes.countryLoaded(undefined));
        this.isMobileView = !isMatched; // full width search bar when screen size less than 768px
      });
  }

  ngOnDestroy() {
    this._destroy$.next();
    this._destroy$.complete();
  }

  ngAfterViewInit() {
    fromEvent(this.searchField.nativeElement, "keyup")
      .pipe(
        takeUntil(this._destroy$),
        distinctUntilChanged(),
        debounceTime(500),
        map<any, string>((e) => e.target.value.toLowerCase()),
        map((search) => {
          if (search && search.length > 1) {
            this._store.dispatch(
              ActionTypes.searchCountry(search.toLowerCase())
            );
          }

          if (!search) {
            this._store.dispatch(ActionTypes.loadCountries());
          }
        })
      )
      .subscribe();
  }

  onCountrySelected(id: string) {
    this.isMobileView
      ? this._router.navigate(["detail", id])
      : this._store.dispatch(ActionTypes.loadCountryById(id));
  }

  clearSearch() {
    this.searchField.nativeElement.value = null;
    this._store.dispatch(ActionTypes.loadCountries());
  }

  private handleError() {
    this._snackBar.open(
      this._transloco.translate("common.could_not_load"),
      "",
      {
        duration: 5000
      }
    );

    this._store.dispatch(ActionTypes.setErrorState({ isError: false }));
  }
}
