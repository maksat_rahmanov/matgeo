import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { TranslocoService } from '@ngneat/transloco';
import { Store } from '@ngrx/store';
import { ActionTypes } from '@store/action.types';
import { Location } from '@angular/common';

@Component({
  selector: 'matgeo-settings',
  templateUrl: './settings.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SettingsComponent implements OnInit {
  currentYear: number;

  constructor(
    private _store: Store,
    private _transloco: TranslocoService,
    private _location: Location
  ) {
    this.currentYear = new Date().getFullYear();
  }

  ngOnInit(): void {
    this._transloco
      .selectTranslate('title', {}, 'settings')
      .subscribe((title) =>
        this._store.dispatch(ActionTypes.setAppTitle(title))
      );
  }

  onBackClick() {
    this._location.back();
  }
}
