import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SettingsRoutingModule } from "./settings-routing.module";
import { SettingsComponent } from "./settings-component/settings.component";
import { SharedModule } from "@shared/shared.module";
import { MatDividerModule } from "@angular/material/divider";
import { TRANSLOCO_SCOPE } from "@ngneat/transloco";

@NgModule({
  declarations: [SettingsComponent],
  providers: [{ provide: TRANSLOCO_SCOPE, useValue: "settings" }],
  imports: [CommonModule, SettingsRoutingModule, SharedModule, MatDividerModule]
})
export class SettingsModule {}
