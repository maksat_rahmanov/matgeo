import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { debounceTime, takeUntil } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { ActionTypes } from '@store/action.types';

@Component({
  selector: 'matgeo-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit, OnDestroy {
  private _destroy$ = new Subject<void>();

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private _store: Store
  ) {}

  ngOnInit() {
    this._activatedRoute.params
      .pipe(takeUntil(this._destroy$), debounceTime(0))
      .subscribe((param) => {
        this._store.dispatch(ActionTypes.setTitleByCountry(param.id));
        this._store.dispatch(ActionTypes.loadCountryById(param.id));
      });
  }

  ngOnDestroy() {
    this._destroy$.next();
    this._destroy$.complete();
  }

  onBackButton() {
    this._store.dispatch(ActionTypes.setCountryId(''));
    this._store.dispatch(ActionTypes.countryLoaded(undefined));
    this._store.dispatch(ActionTypes.loadCountries());
    this._router.navigate(['/']);
  }
}
