import { ChangeDetectionStrategy, Component } from "@angular/core";

@Component({
  selector: "matgeo-not-found",
  template: `
    <matgeo-action-bar>
      <button class="menu-action" mat-icon-button routerLink="/">
        <mat-icon>west</mat-icon>
      </button>
    </matgeo-action-bar>

    <div class="flex flex-col w-full h-full items-center mt-32">
      <mat-icon class="icon muted-icon">travel_explore</mat-icon>
      <h3 class="my-8 text-lg">{{ "common.page_not_found" | transloco }}</h3>
      <button
        mat-fab
        color="warn"
        routerLink="/"
        title="{{ 'common.back_to_home_page' | transloco }}"
      >
        <mat-icon>home</mat-icon>
      </button>
    </div>
  `,
  styleUrls: ["./page-not-found.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PageNotFoundComponent {}
