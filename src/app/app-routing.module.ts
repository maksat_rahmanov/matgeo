import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { HomeComponent } from "./pages/home/home.component";
import { PageNotFoundComponent } from "./pages/page-not-found/page-not-found.component";

const routes: Routes = [
  {
    path: "",
    component: HomeComponent
  },
  {
    path: "detail",
    loadChildren: () =>
      import("./pages/detail/detail.module").then(
        (module) => module.DetailModule
      )
  },
  /*{
    path: "search",
    loadChildren: () =>
      import("./pages/advanced-search/advanced-search.module").then(
        (module) => module.AdvancedSearchModule
      ),
  },*/
  {
    path: "settings",
    loadChildren: () =>
      import("./pages/settings/settings.module").then(
        (module) => module.SettingsModule
      )
  },
  {
    path: "not-found",
    component: PageNotFoundComponent
  },
  {
    path: "**",
    redirectTo: "not-found"
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
