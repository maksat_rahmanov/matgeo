import { CountryDto } from '../dto/country.dto';
import { BorderCountry } from './border-country';

export class Country implements AbstractModel {
  id: string;
  name: string;
  capital: string;
  region?: string;
  subregion?: string;
  flag?: string;
  heroImage?: string;
  population?: number;
  area?: number;
  latitude?: number;
  longitude?: number;
  alpha2Code?: string;
  alpha3Code?: string;
  topLevelDomain?: string;
  callingCodes?: string[];
  timeZones?: string[];
  currencies?: string[];
  languages?: string[];
  borderCountries?: BorderCountry[];

  constructor(countryDto: CountryDto) {
    this.id = countryDto.id;
    this.name = countryDto.name;
    this.capital = countryDto.capital;
    this.region = countryDto.region;
    this.subregion = countryDto.subregion;
    this.flag = countryDto.flag;
    this.heroImage = countryDto.heroImage;
    this.area = countryDto.area;
    this.population = countryDto.population;
    this.latitude = countryDto.latitude;
    this.longitude = countryDto.longitude;
    this.alpha2Code = countryDto.alpha2Code;
    this.alpha3Code = countryDto.alpha3Code;
    this.topLevelDomain = countryDto.topLevelDomain;
    this.callingCodes = countryDto.callingCodes;
    this.languages = countryDto.languages;
    this.timeZones = countryDto.timeZones;
    this.currencies = countryDto.currencies;
    this.borderCountries = countryDto.borderCountries;
  }
}
