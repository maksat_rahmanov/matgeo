import { BorderCountryDto } from '../dto/border-country.dto';

export class BorderCountry {
  id: string;
  name: string;
  flag: string;

  constructor(borderCountryDto: BorderCountryDto) {
    this.id = borderCountryDto.id;
    this.name = borderCountryDto.name;
    this.flag = borderCountryDto.flag;
  }
}
