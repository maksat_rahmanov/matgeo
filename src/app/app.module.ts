import { HttpClientModule } from "@angular/common/http";
import { APP_INITIALIZER, LOCALE_ID, NgModule } from "@angular/core";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { GraphcmsModule } from "./graphcms/graphcms.module";
import { HomeComponent } from "./pages/home/home.component";
import { LayoutModule } from "@angular/cdk/layout";
import { SharedModule } from "@shared/shared.module";
import { MatSnackBarModule } from "@angular/material/snack-bar";
import { MatInputModule } from "@angular/material/input";
import { MatTooltipModule } from "@angular/material/tooltip";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { DOCUMENT, registerLocaleData } from "@angular/common";
import { Store, StoreModule } from "@ngrx/store";
import { EffectsModule } from "@ngrx/effects";
import { StoreDevtoolsModule } from "@ngrx/store-devtools";
import { environment } from "../environments/environment";
import { BrowserModule } from "@angular/platform-browser";
import { StoreEffects } from "@store/effects";
import { appReducer, countriesReducer } from "@store/reducers";
import { getLanguage, initApp } from "./app.init";
import { PageNotFoundComponent } from "./pages/page-not-found/page-not-found.component";

import localeEn from "@angular/common/locales/en-GB";
import localeRu from "@angular/common/locales/ru";
import localeEnExtra from "@angular/common/locales/extra/en-GB";
import localeRuExtra from "@angular/common/locales/extra/ru";

registerLocaleData(localeEn, "en", localeEnExtra);
registerLocaleData(localeRu, "ru", localeRuExtra);

const storeDevTools = !environment.production
  ? [
      StoreDevtoolsModule.instrument({
        maxAge: 25,
        logOnly: environment.production
      })
    ]
  : [];

@NgModule({
  declarations: [AppComponent, HomeComponent, PageNotFoundComponent],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    GraphcmsModule,
    MatSnackBarModule,
    MatInputModule,
    MatTooltipModule,
    SharedModule,
    LayoutModule,
    StoreModule.forRoot({ countries: countriesReducer, app: appReducer }),
    EffectsModule.forRoot([StoreEffects]),
    ...storeDevTools
  ],
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory: initApp,
      deps: [Store, DOCUMENT],
      multi: true
    },
    {
      provide: LOCALE_ID,
      useFactory: getLanguage,
      deps: [DOCUMENT]
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
