import { BorderCountry } from "../model/border-country";
import { Country } from "../model/country";
import { buildImageUrl } from "@shared/helpers/image-url.builder";
import { ContentType } from "./content-type";

export class CountryContentType implements ContentType {
  id: string;
  name: string;
  capital: string;
  region?: Region;
  subregion?: Region;
  area?: number;
  population?: number;
  flag?: Asset;
  heroImage?: Asset;
  location?: Location;
  alpha2Code?: string;
  alpha3Code?: string;
  topLevelDomain?: string[];
  callingCodes?: string[];
  timeZones?: string[];
  currencies?: Currency[];
  languages?: Language[];
  borderCountries?: NeighborCountry[];

  readonly hero_image_width = 768;

  constructor(contentType: CountryContentType) {
    this.id = contentType.id;
    this.name = contentType.name;
    this.capital = contentType.capital;
    this.region = contentType.region;
    this.subregion = contentType.subregion;
    this.flag = contentType.flag;
    this.heroImage = contentType.heroImage;
    this.area = contentType.area;
    this.population = contentType.population;
    this.location = contentType.location;
    this.alpha2Code = contentType.alpha2Code;
    this.alpha3Code = contentType.alpha3Code;
    this.topLevelDomain = contentType.topLevelDomain;
    this.callingCodes = contentType.callingCodes;
    this.timeZones = contentType.timeZones;
    this.languages = contentType.languages;
    this.currencies = contentType.currencies;
    this.borderCountries = contentType.borderCountries;
  }

  mapToModel(): Country {
    return new Country({
      id: this.id,
      name: this.name,
      capital: this.capital,
      region: this.region?.name,
      subregion: this.subregion?.name,
      population: this.population,
      area: this.area,
      flag: this.flag?.url,
      heroImage: this.heroImage
        ? buildImageUrl(
            this.heroImage?.mimeType,
            this.heroImage?.handle,
            this.hero_image_width
          )
        : undefined,
      latitude: this.location?.latitude,
      longitude: this.location?.longitude,
      alpha2Code: this.alpha2Code,
      alpha3Code: this.alpha3Code,
      callingCodes: this.callingCodes?.map((code) => `+${code}`),
      topLevelDomain: this.topLevelDomain
        ?.map((domain) => `.${domain}`)
        .join(", "),
      timeZones: this.timeZones,
      currencies: this.currencies?.map(({ code, name, symbol }) =>
        `${symbol || ""} ${name} (${code})`.trim()
      ),
      languages: this.languages?.map(({ name, code }) => `${name} (${code})`),
      borderCountries: this.borderCountries?.map(
        ({ id, name, flag }) =>
          new BorderCountry({
            id,
            name,
            flag: flag.url
          })
      )
    });
  }
}
