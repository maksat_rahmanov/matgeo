import { Component, Inject, LOCALE_ID, OnInit } from '@angular/core';
import { DOCUMENT, LocationStrategy } from '@angular/common';
import { Store } from '@ngrx/store';
import { selectTheme } from '@store/selectors';
import { TranslocoService } from '@ngneat/transloco';
import { ActionTypes } from './store/action.types';

@Component({
  selector: 'matgeo-app',
  template: '<router-outlet></router-outlet>'
})
export class AppComponent implements OnInit {
  constructor(
    private _locationStrategy: LocationStrategy,
    private _store: Store,
    private _transloco: TranslocoService,
    @Inject(DOCUMENT) private _document: Document,
    @Inject(LOCALE_ID) private _locale: string
  ) {}

  ngOnInit(): void {
    this._locationStrategy.onPopState(() => {
      history.pushState(null, '', window.location.href);
      this._store.dispatch(ActionTypes.setCountryId(''));
    });

    this._transloco.setActiveLang(this._locale);
    this._store
      .select(selectTheme)
      .subscribe(
        (theme) =>
          (this._document.body.className = `${theme} mat-app-background`)
      );
  }
}
