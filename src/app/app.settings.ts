export const LANGUAGES = new Map<string, string>([
  ['en', 'common.lang_en'],
  ['ru', 'common.lang_ru']
]);

export const THEMES = new Map<string, string>([
  ['dark', 'common.theme_dark'],
  ['light', 'common.theme_light']
]);
