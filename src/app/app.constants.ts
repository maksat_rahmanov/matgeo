export class Constants {
  static readonly appTitle = "Material Geography";
  static readonly themeCookie = "mat-geo-theme";
  static readonly languageCookie = "mat-geo-lang";
  static readonly listPageSize = 50;
  static readonly assetsUrl = "https://media.graphcms.com";
}
