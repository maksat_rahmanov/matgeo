import { NgModule } from "@angular/core";
import { CommonModule, NgOptimizedImage } from "@angular/common";
import { CountryListComponent } from "./components/country-list/country-list.component";
import { CountryDetailComponent } from "./components/country-detail/country-detail.component";
import { CountryDetailSmComponent } from "./components/country-detail-sm/country-detail-sm.component";
import { CountryDetailMdComponent } from "./components/country-detail-md/country-detail-md.component";
import { CountryDetailLgComponent } from "./components/country-detail-lg/country-detail-lg.component";
import { MatListModule } from "@angular/material/list";
import { MatIconModule } from "@angular/material/icon";
import { MatTabsModule } from "@angular/material/tabs";
import { MatButtonModule } from "@angular/material/button";
import { MatExpansionModule } from "@angular/material/expansion";
import { ActionBarComponent } from "./components/action-bar/action-bar.component";
import { MatToolbarModule } from "@angular/material/toolbar";
import { MatChipsModule } from "@angular/material/chips";
import { MatCardModule } from "@angular/material/card";
import { MatTooltipModule } from "@angular/material/tooltip";
import { RouterModule } from "@angular/router";
import { IconListComponent } from "./components/icon-list/icon-list.component";
import {
  BorderCountryListComponent,
  DetailsExpandableComponent,
  ExpandableBaseComponent
} from "./components/expandable-list/expandable-list.component";
import { AgmCoreModule } from "@diegomvh/agm-core";
import { environment } from "../../environments/environment";
import { PreviewComponent } from "./components/preview/preview.component";
import { MatMenuModule } from "@angular/material/menu";
import { TranslocoRootModule } from "../transloco-root.module";
import { MatRadioModule } from "@angular/material/radio";
import { SelectLanguageComponent } from "./components/select-language/select-language.component";
import { CountryInfoGenerator } from "./components/generator/country-info.generator";
import { SelectThemeComponent } from "./components/select-theme/select-theme.component";
import { MatBadgeModule } from "@angular/material/badge";
import { MatProgressSpinnerModule } from "@angular/material/progress-spinner";
import { CommonSelectMenuComponent } from "./components/common-select-menu/common-select-menu.component";
import { LanguageService } from "./services/language.service";
import { ThemeService } from "./services/theme.service";
import { InfiniteScrollModule } from "ngx-infinite-scroll";

@NgModule({
  declarations: [
    CountryListComponent,
    CountryDetailComponent,
    CountryDetailSmComponent,
    CountryDetailMdComponent,
    CountryDetailLgComponent,
    ActionBarComponent,
    IconListComponent,
    DetailsExpandableComponent,
    BorderCountryListComponent,
    PreviewComponent,
    SelectLanguageComponent,
    SelectThemeComponent,
    CommonSelectMenuComponent,
    ExpandableBaseComponent
  ],
  imports: [
    CommonModule,
    MatToolbarModule,
    MatListModule,
    MatIconModule,
    MatTabsModule,
    MatButtonModule,
    MatExpansionModule,
    MatChipsModule,
    MatCardModule,
    MatMenuModule,
    MatRadioModule,
    MatProgressSpinnerModule,
    MatBadgeModule,
    MatTooltipModule,
    RouterModule,
    TranslocoRootModule,
    NgOptimizedImage,
    InfiniteScrollModule,
    AgmCoreModule.forRoot({
      apiKey: environment.mapsApiKey
    })
  ],
  exports: [
    CountryListComponent,
    CountryDetailComponent,
    ActionBarComponent,
    SelectLanguageComponent,
    MatButtonModule,
    MatIconModule,
    TranslocoRootModule,
    SelectThemeComponent,
    PreviewComponent
  ],
  providers: [CountryInfoGenerator, LanguageService, ThemeService]
})
export class SharedModule {}
