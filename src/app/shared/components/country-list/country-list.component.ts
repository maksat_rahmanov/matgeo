import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  OnDestroy,
  OnInit,
  Output
} from "@angular/core";
import { Country } from "../../../model/country";
import { ImageLoadAnimationHandler } from "@shared/helpers/image-animation";
import { selectAllCountries } from "@store/selectors";
import { Store } from "@ngrx/store";
import { Observable, Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { ActionTypes } from "@store/action.types";

@Component({
  selector: "matgeo-country-list",
  templateUrl: "./country-list.component.html",
  styleUrls: ["./country-list.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CountryListComponent
  extends ImageLoadAnimationHandler
  implements OnInit, OnDestroy
{
  @Output("selectedId") countryIdEmitter = new EventEmitter<string>();

  countries$!: Observable<Country[]>;
  selectedCountryId: string = "";

  private _destroy$ = new Subject<void>();

  constructor(private _store: Store) {
    super();
  }

  ngOnInit() {
    this.countries$ = this._store
      .select(selectAllCountries)
      .pipe(takeUntil(this._destroy$));
  }

  ngOnDestroy() {
    this._destroy$.next();
    this._destroy$.complete();
  }

  onItemClick(id: string) {
    this.selectedCountryId = id;
    this.countryIdEmitter.emit(id);
  }

  loadCountries() {
    this._store.dispatch(ActionTypes.loadNextCountries());
  }
}
