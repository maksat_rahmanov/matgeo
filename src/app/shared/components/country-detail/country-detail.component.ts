import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ComponentRef,
  OnDestroy,
  OnInit,
  ViewChild,
  ViewContainerRef
} from "@angular/core";
import { BreakpointObserver } from "@angular/cdk/layout";
import { debounceTime, map, takeUntil } from "rxjs/operators";
import { CountryDetailSmComponent } from "../country-detail-sm/country-detail-sm.component";
import { CountryDetailMdComponent } from "../country-detail-md/country-detail-md.component";
import { CountryDetailLgComponent } from "../country-detail-lg/country-detail-lg.component";
import { Subject } from "rxjs";

@Component({
  selector: 'matgeo-country-detail',
  template: `<ng-template #dynamicContent></ng-template>`,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CountryDetailComponent implements OnInit, OnDestroy {
  @ViewChild('dynamicContent', { read: ViewContainerRef, static: false })
  dynamicContent!: ViewContainerRef;

  private _componentRef!: ComponentRef<any>;
  private _destroy$ = new Subject<void>();

  private _componentSizeMap: any = {
    '(min-width: 680px)': CountryDetailSmComponent,
    '(min-width: 768px)': CountryDetailMdComponent,
    '(min-width: 1280px)': CountryDetailLgComponent
  };

  constructor(
    private _breakpointObserver: BreakpointObserver,
    private _cdr: ChangeDetectorRef
  ) {}

  ngOnInit(): void {
    this._breakpointObserver
      .observe(Array.from(Object.keys(this._componentSizeMap)))
      .pipe(
        takeUntil(this._destroy$),
        map((state) => {
          const matchedBreakPoints = Object.keys(state.breakpoints).filter(
            (key) => state.breakpoints[key]
          );
          return matchedBreakPoints[matchedBreakPoints.length - 1];
        }),
        debounceTime(10)
      )
      .subscribe((breakpoint) => {
        this.dynamicContent.clear();
        const component = this._componentSizeMap[breakpoint] || CountryDetailSmComponent;

        this._componentRef = this.dynamicContent.createComponent(component);
        this._cdr.detectChanges();
      });
  }

  ngOnDestroy(): void {
    this._destroy$.next();
    this._destroy$.complete();
  }
}
