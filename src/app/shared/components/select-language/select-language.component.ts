import { Component, Inject, LOCALE_ID, OnInit } from '@angular/core';
import { LANGUAGES } from 'src/app/app.settings';
import { LanguageService } from '../../services/language.service';

@Component({
  selector: 'matgeo-select-language',
  template: `<matgeo-common-select-menu
    [menuItems]="languages"
    [currentValue]="currentLocale"
    [onItemSelect]="switchLanguage"
  ></matgeo-common-select-menu>`
})
export class SelectLanguageComponent implements OnInit {
  languages = LANGUAGES;
  currentLocale!: string;

  constructor(
    private _languageService: LanguageService,
    @Inject(LOCALE_ID) private _locale: string
  ) {}

  ngOnInit(): void {
    this.currentLocale = this._locale;
  }

  switchLanguage = (language: string): void => {
    this._languageService.setLanguage(language);
  };
}
