import { CountryInfoGenerator, ExtendedItem } from './country-info.generator';

export interface GroupedDetailItems {
  basicInfoList: DetailItem[];
  detailInfoList: DetailItem[];
  expandableItems: DetailItem[];
  moreInfoList?: DetailItem[];
}

export abstract class DetailInfoTemplate {
  abstract getDetailInfo(): GroupedDetailItems;

  protected basicInfoList: DetailItem[] = [];
  protected detailInfoList: DetailItem[] = [];
  protected readonly itemsToShow = 2; // In case of multiple values show optimal items count per row

  private expandableItems: DetailItem[] = [];
  private moreInfoList: DetailItem[] = [];
  private mapSettings?: MapSettings;
  private extendedItems: ExtendedItem[] = [];

  protected constructor(infoGenerator: CountryInfoGenerator) {
    this.basicInfoList = infoGenerator.getBasicInfo();
    this.detailInfoList = infoGenerator.getDetailInfo();
    this.extendedItems = infoGenerator.getExtendedInfo();
    this.mapSettings = infoGenerator.getMapSettings();
  }

  getMapSettings(): MapSettings | undefined {
    return this.mapSettings;
  }

  protected get basicInfo(): DetailItem[] {
    return this.basicInfoList.filter((it) => it.title);
  }

  protected get detailInfo(): DetailItem[] {
    return this.detailInfoList.filter((it) => it.title);
  }

  protected get expandableInfo(): DetailItem[] {
    return this.expandableItems;
  }

  protected get moreInfo(): DetailItem[] {
    return this.moreInfoList;
  }

  protected setExtendedDetailItems(isComplex = false): void {
    this.extendedItems.forEach((it) => {
      this.generateExtendedDetailItem(
        it.values,
        it.subtitle,
        it.icon,
        isComplex
      );
    });
  }

  private generateExtendedDetailItem(
    values: string[] | undefined,
    subtitle: string,
    icon: string,
    isComplex: boolean
  ): void {
    let itemDetail: DetailItem = { subtitle, icon };

    // TS strictTypes mode requires such condition
    if (values && values.length < this.itemsToShow) {
      itemDetail.title = values?.join(', ') || '';
      isComplex
        ? this.moreInfoList.push(itemDetail)
        : this.detailInfoList.push(itemDetail);
    } else {
      itemDetail.items = values;
      this.expandableItems.push(itemDetail);
    }
  }
}

export class DetailInfoGeneratorSm extends DetailInfoTemplate {
  constructor(infoGenerator: CountryInfoGenerator) {
    super(infoGenerator);
  }

  getDetailInfo(): GroupedDetailItems {
    this.setExtendedDetailItems();

    return {
      basicInfoList: this.basicInfo,
      detailInfoList: this.detailInfo,
      expandableItems: this.expandableInfo
    };
  }
}

export class DetailInfoGeneratorMd extends DetailInfoTemplate {
  constructor(infoGenerator: CountryInfoGenerator) {
    super(infoGenerator);
  }

  getDetailInfo(): GroupedDetailItems {
    this.setExtendedDetailItems(true);
    this.basicInfoList.splice(0, 1);

    return {
      basicInfoList: this.basicInfo,
      detailInfoList: this.detailInfo,
      expandableItems: this.expandableInfo,
      moreInfoList: this.moreInfo
    };
  }
}

export class DetailInfoGeneratorLg extends DetailInfoTemplate {
  constructor(infoGenerator: CountryInfoGenerator) {
    super(infoGenerator);
  }

  getDetailInfo(): GroupedDetailItems {
    this.setExtendedDetailItems();
    this.basicInfoList.splice(0, 1);
    this.basicInfoList.push(this.detailInfo[0]);
    this.detailInfoList.splice(0, 4);

    return {
      basicInfoList: this.basicInfo,
      detailInfoList: this.detailInfo,
      expandableItems: this.expandableInfo
    };
  }
}
