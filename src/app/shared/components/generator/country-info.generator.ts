import { formatNumber } from '@angular/common';
import { Inject, Injectable, LOCALE_ID } from '@angular/core';
import { TranslocoService } from '@ngneat/transloco';
import { Country } from 'src/app/model/country';

export interface ExtendedItem {
  values: string[] | undefined;
  subtitle: string;
  icon: string;
}

@Injectable()
export class CountryInfoGenerator {
  private _country!: Country;

  constructor(
    private _transloco: TranslocoService,
    @Inject(LOCALE_ID) private _locale: string
  ) {}

  setCountry(country: Country): void {
    this._country = country;
  }

  getMapSettings(): MapSettings | undefined {
    if (this._country.latitude && this._country.longitude) {
      return {
        lat: this._country.latitude,
        lon: this._country.longitude,
        zoom: 5,
        minZoom: 2,
        maxZoom: 7
      };
    }

    return undefined;
  }

  getBasicInfo(): DetailItem[] {
    return [
      {
        title: this._country.capital,
        subtitle: this._transloco.translate('details.capital'),
        icon: 'flag'
      },
      {
        title: this._country.region,
        subtitle: this._transloco.translate('details.region'),
        icon: 'map'
      },
      {
        title: this._country.subregion,
        subtitle: this._transloco.translate('details.subregion'),
        icon: 'place'
      },
      {
        title: this._transloco.translate('details.area_sq_km', {
          area: formatNumber(this._country.area ?? 0, this._locale)
        }),
        subtitle: this._transloco.translate('details.area'),
        icon: 'panorama_horizontal'
      },
      {
        title: formatNumber(this._country.population ?? 0, this._locale),
        subtitle: this._transloco.translate('details.population'),
        icon: 'people'
      },
      {
        title: this._country.topLevelDomain,
        subtitle: this._transloco.translate('details.top_level_domain'),
        icon: 'domain'
      }
    ];
  }

  getDetailInfo(): DetailItem[] {
    const items = [
      {
        title: this._country.callingCodes?.join(', '),
        subtitle: this._transloco.translate('details.calling_codes'),
        icon: 'call'
      },
      {
        title: this._country.alpha2Code,
        subtitle: this._transloco.translate('details.alpha_two_code'),
        icon: 'qr_code'
      },
      {
        title: this._country.alpha3Code,
        subtitle: this._transloco.translate('details.alpha_three_code'),
        icon: 'qr_code'
      }
    ];

    if (this._country.latitude && this._country.longitude) {
      items.push({
        title: this._transloco.translate('details.lat_lon', {
          lat: formatNumber(this._country.latitude, this._locale, '2.2-4'),
          lon: formatNumber(this._country.longitude, this._locale, '2.2-4')
        }),
        subtitle: this._transloco.translate('details.coordinates'),
        icon: 'my_location'
      });
    }

    return items;
  }

  getExtendedInfo(): ExtendedItem[] {
    return [
      {
        values: this._country.currencies,
        subtitle: this._transloco.translate('details.currencies'),
        icon: 'money'
      },
      {
        values: this._country.languages,
        subtitle: this._transloco.translate('details.languages'),
        icon: 'translate'
      },
      {
        values: this._country.timeZones,
        subtitle: this._transloco.translate('details.time_zones'),
        icon: 'schedule'
      }
    ];
  }
}
