import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component
} from '@angular/core';
import { CountryInfoGenerator } from '../generator/country-info.generator';
import {
  DetailInfoGeneratorMd,
  DetailInfoTemplate,
  GroupedDetailItems
} from '../generator/detail-info.template';
import { AbstractCountryDetail } from '../../helpers/abstract.country-detail';
import { Store } from '@ngrx/store';
import { Country } from '../../../model/country';

@Component({
  selector: 'matgeo-country-detail-md',
  templateUrl: './country-detail-md.component.html',
  styleUrls: ['./country-detail-md.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CountryDetailMdComponent extends AbstractCountryDetail {
  name!: string;
  capital!: string;
  flag?: string;
  moreInfoList: DetailItem[] = [];

  constructor(
    _store: Store,
    _countryInfoGenerator: CountryInfoGenerator,
    _cdr: ChangeDetectorRef
  ) {
    super(_store, _countryInfoGenerator, _cdr);
  }

  setInitialInfo(country: Country) {
    this.name = country.name;
    this.capital = country.capital;
    this.flag = country.flag;
  }

  initDetailInfoGenerator(
    infoGenerator: CountryInfoGenerator
  ): DetailInfoTemplate {
    return new DetailInfoGeneratorMd(infoGenerator);
  }

  setTabInfo(tabsInfo: GroupedDetailItems) {
    super.setTabInfo(tabsInfo);
    this.moreInfoList = tabsInfo.moreInfoList || [];
  }
}
