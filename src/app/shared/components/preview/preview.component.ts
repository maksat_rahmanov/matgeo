import { Component } from "@angular/core";

@Component({
  selector: "matgeo-preview",
  template: `
    <mat-icon class="icon muted-icon mb-8">travel_explore</mat-icon>
    <div>
      <p class="text muted-text">
        <mat-icon class="mr-4" color="primary">north_east</mat-icon>
        {{ "preview.quick_find" | transloco }}
      </p>
      <p class="text muted-text">
        <mat-icon class="mr-4" color="primary">west</mat-icon>
        {{ "preview.see_details" | transloco }}
      </p>
      <!--p class="text muted-text">
        <mat-icon class="mr-4" color="primary">south_east</mat-icon>
        {{ "preview.advanced_search" | transloco }}
      </p-->
    </div>
  `,
  styleUrls: ["./preview.component.scss"]
})
export class PreviewComponent {}
