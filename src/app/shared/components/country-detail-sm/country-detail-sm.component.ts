import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component
} from "@angular/core";
import { CountryInfoGenerator } from "../generator/country-info.generator";
import {
  DetailInfoGeneratorSm,
  DetailInfoTemplate
} from "../generator/detail-info.template";
import { AbstractCountryDetail } from "../../helpers/abstract.country-detail";
import { Country } from "../../../model/country";
import { Store } from "@ngrx/store";

@Component({
  selector: "matgeo-country-detail-sm",
  templateUrl: "./country-detail-sm.component.html",
  styleUrls: ["./country-detail-sm.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CountryDetailSmComponent extends AbstractCountryDetail {
  flag?: string;
  heroImage?: string;

  constructor(
    private _store: Store,
    _countryInfoGenerator: CountryInfoGenerator,
    _cdr: ChangeDetectorRef
  ) {
    super(_store, _countryInfoGenerator, _cdr);
  }

  setInitialInfo(country: Country) {
    this.flag = country.flag;
    this.heroImage = country.heroImage;
  }

  initDetailInfoGenerator(
    infoGenerator: CountryInfoGenerator
  ): DetailInfoTemplate {
    return new DetailInfoGeneratorSm(infoGenerator);
  }
}
