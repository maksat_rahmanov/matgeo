import { Component, Input, OnInit } from '@angular/core';

interface CommonMenuItem {
  code: string;
  name: string;
}

@Component({
  selector: 'matgeo-common-select-menu',
  template: `
    <mat-radio-group class="flex flex-col">
      <mat-radio-button
        class="mr-2 mt-2"
        *ngFor="let item of items"
        [checked]="item.code === currentValue"
        [value]="item"
        (change)="onItemSelect(item.code)"
      >
        {{ item.name | transloco }}
      </mat-radio-button>
    </mat-radio-group>`
})
export class CommonSelectMenuComponent implements OnInit {
  @Input({required: true}) menuItems: Map<string, string> = new Map([]);
  @Input({required: true}) currentValue: string = '';
  @Input({required: true}) onItemSelect!: (itemCode: string) => void;

  items: CommonMenuItem[] = [];

  ngOnInit(): void {
    this.items = Array.from(this.menuItems.entries()).map((menuItem) => {
      const [code, name] = menuItem;
      return { code, name };
    });
  }
}
