import { ChangeDetectorRef, Component } from "@angular/core";
import { CountryInfoGenerator } from "../generator/country-info.generator";
import {
  DetailInfoGeneratorLg,
  DetailInfoTemplate
} from "../generator/detail-info.template";
import { AbstractCountryDetail } from "../../helpers/abstract.country-detail";
import { Store } from "@ngrx/store";
import { Country } from "../../../model/country";

@Component({
  selector: "matgeo-country-detail-lg",
  templateUrl: "./country-detail-lg.component.html",
  styleUrls: ["./country-detail-lg.component.scss"]
})
export class CountryDetailLgComponent extends AbstractCountryDetail {
  name!: string;
  capital!: string;
  mapSettings?: MapSettings;
  flag?: string;
  heroImage?: string;
  alpha2Code?: string;
  alpha3Code?: string;

  constructor(
    _store: Store,
    _countryInfoGenerator: CountryInfoGenerator,
    _cdr: ChangeDetectorRef
  ) {
    super(_store, _countryInfoGenerator, _cdr);
  }

  setInitialInfo(country: Country) {
    this.name = country.name;
    this.capital = country.capital;
    this.flag = country.flag;
    this.heroImage = country.heroImage;
    this.alpha2Code = country.alpha2Code;
    this.alpha3Code = country.alpha3Code;
  }

  initDetailInfoGenerator(
    infoGenerator: CountryInfoGenerator
  ): DetailInfoTemplate {
    return new DetailInfoGeneratorLg(infoGenerator);
  }
}
