import { Component, Input, OnInit } from "@angular/core";
import { Observable } from "rxjs";
import { Store } from "@ngrx/store";
import { selectAppTitle, selectLoading } from "@store/selectors";

@Component({
  selector: "matgeo-action-bar",
  templateUrl: "./action-bar.component.html",
  styleUrls: ["./action-bar.component.scss"]
})
export class ActionBarComponent implements OnInit {
  @Input() disableSettings!: boolean;
  title$!: Observable<string>;
  isLoading$!: Observable<boolean>;

  constructor(private _store: Store) {}

  ngOnInit(): void {
    this.title$ = this._store.select(selectAppTitle);
    this.isLoading$ = this._store.select(selectLoading);
  }
}
