import { ChangeDetectionStrategy, Component, Input } from "@angular/core";
import { BorderCountry } from "../../../model/border-country";

@Component({
  selector: "matgeo-details-expandable",
  template: `<ng-container *ngFor="let detailItem of items">
    <ng-template [ngIf]="detailItem?.items?.length">
      <matgeo-expandable
        [items]="detailItem.items"
        [title]="detailItem.subtitle"
        [icon]="detailItem.icon"
      >
        <mat-chip-listbox class="ml-12">
          <mat-chip *ngFor="let item of detailItem.items">{{ item }}</mat-chip>
        </mat-chip-listbox>
      </matgeo-expandable>
    </ng-template>
  </ng-container>`,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DetailsExpandableComponent {
  @Input({ required: true }) items: DetailItem[] = [];
}

@Component({
  selector: "matgeo-border-country-list",
  template: `<ng-container *ngIf="items.length">
    <matgeo-expandable
      [items]="items"
      [title]="'details.border_countries' | transloco"
      icon="border_outer"
    >
      <mat-chip-listbox class="ml-12">
        <mat-chip *ngFor="let country of items">
          <img
            *ngIf="!!country.flag"
            matChipAvatar
            class="object-cover"
            width="18"
            height="18"
            [ngSrc]="country.flag"
            [alt]="country.name + ' flag'"
          />
          {{ country.name }}</mat-chip
        >
      </mat-chip-listbox>
    </matgeo-expandable>
  </ng-container>`,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BorderCountryListComponent {
  @Input({ required: true }) items: BorderCountry[] = [];
}

@Component({
  selector: "matgeo-expandable",
  templateUrl: "./expandable-list.component.html",
  styleUrls: ["./expandable-list.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ExpandableBaseComponent<T> {
  @Input({ required: true }) items: T[] | undefined = [];
  @Input({ required: true }) title = "";
  @Input() icon = "";
}
