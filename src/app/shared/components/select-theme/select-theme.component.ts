import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { THEMES } from 'src/app/app.settings';
import { ThemeService } from '../../services/theme.service';

@Component({
  selector: 'matgeo-select-theme',
  template: `<ng-container *ngIf="currentTheme$ | async as currentTheme">
    <matgeo-common-select-menu
      [menuItems]="themes"
      [currentValue]="currentTheme"
      [onItemSelect]="switchTheme"
    ></matgeo-common-select-menu>
  </ng-container>`
})
export class SelectThemeComponent implements OnInit {
  themes = THEMES;
  currentTheme$!: Observable<string>;

  constructor(private _themeService: ThemeService) {}

  ngOnInit(): void {
    this.currentTheme$ = this._themeService.getCurrentTheme();
  }

  switchTheme = (theme: string): void => {
    this._themeService.setTheme(theme);
  };
}
