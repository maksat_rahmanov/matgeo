import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'matgeo-icon-list',
  template: `<mat-list>
    <mat-list-item *ngFor="let item of items; last as last">
      <mat-icon matListItemIcon>{{ item.icon }}</mat-icon>
      <h4 matListItemTitle>{{ item.title }}</h4>
      <span class="text-sm inline-block mt-2">{{ item.subtitle }}</span>
      <mat-divider inset *ngIf="!last"></mat-divider>
    </mat-list-item>
  </mat-list>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class IconListComponent {
  @Input() items: DetailItem[] = [];
}
