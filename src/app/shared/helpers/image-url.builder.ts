import { Constants } from "../../app.constants";

const mimeTypes: ReadonlyArray<string> = [
  "image/jpeg",
  "image/jpg",
  "image/png",
  "image/tiff",
  "image/webp"
];

export function buildImageUrl(
  mimeType?: string,
  imageId?: string,
  maxWidth?: number
): string {
  if (mimeTypes.includes(mimeType || "") && maxWidth) {
    return `${Constants.assetsUrl}/resize=fit:max,width:${maxWidth}/${imageId}`;
  }

  return "";
}
