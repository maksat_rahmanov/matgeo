import { ChangeDetectorRef, Directive, OnDestroy, OnInit } from "@angular/core";
import { Country } from "../../model/country";
import { CountryInfoGenerator } from "../components/generator/country-info.generator";
import {
  DetailInfoTemplate,
  GroupedDetailItems
} from "../components/generator/detail-info.template";
import { takeUntil } from "rxjs/operators";
import { Subject } from "rxjs";
import { BorderCountry } from "../../model/border-country";
import { Store } from "@ngrx/store";
import { selectCountry } from "@store/selectors";
import { ImageLoadAnimationHandler } from "@shared/helpers/image-animation";

@Directive()
export abstract class AbstractCountryDetail
  extends ImageLoadAnimationHandler
  implements OnInit, OnDestroy
{
  basicInfoList: DetailItem[] = [];
  detailInfoList: DetailItem[] = [];
  expandableItems: DetailItem[] = [];
  borderCountries: BorderCountry[] = [];
  mapSettings?: MapSettings;

  private _destroy$ = new Subject<void>();

  protected constructor(
    private _appStore: Store,
    private _countryInfoGenerator: CountryInfoGenerator,
    private _cdr: ChangeDetectorRef
  ) {
    super();
  }

  ngOnInit(): void {
    this.initData();
  }

  ngOnDestroy(): void {
    this._destroy$.next();
    this._destroy$.complete();
  }

  abstract setInitialInfo(country: Country): void;

  abstract initDetailInfoGenerator(
    infoGenerator: CountryInfoGenerator
  ): DetailInfoTemplate;

  protected setTabInfo(tabsInfo: GroupedDetailItems): void {
    this.basicInfoList = tabsInfo.basicInfoList;
    this.detailInfoList = tabsInfo.detailInfoList;
    this.expandableItems = tabsInfo.expandableItems;
  }

  protected initData(): void {
    this._appStore
      .select(selectCountry)
      .pipe(takeUntil(this._destroy$))
      .subscribe((country) => {
        if (!country) {
          return;
        }

        this.setInitialInfo(country);
        this._countryInfoGenerator.setCountry(country);

        const infoGenerator = this.initDetailInfoGenerator(
          this._countryInfoGenerator
        );
        this.mapSettings = infoGenerator.getMapSettings();

        const tabsInfo = infoGenerator.getDetailInfo();
        this.setTabInfo(tabsInfo);

        this.borderCountries = country.borderCountries || [];

        this._cdr.detectChanges();
      });
  }
}
