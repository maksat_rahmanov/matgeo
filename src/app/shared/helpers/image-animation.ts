export class ImageLoadAnimationHandler {
  protected onImageLoad(element: HTMLImageElement) {
    element.classList.remove("image-loading");
  }
}
