export abstract class AbstractCookieService {
  private readonly _expirationDays: number = 30;
  private _date = new Date();

  protected constructor() {
    const expDaysInMs = this._expirationDays * 24 * 3600 * 1000;
    this._date.setTime(this._date.getTime() + expDaysInMs);
  }

  protected setCookie(key: string, value: string): void {
    document.cookie = `${key}=${value};expires=${this._date.toUTCString()};path=/`;
  }
}
