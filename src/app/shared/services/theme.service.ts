import { Injectable } from '@angular/core';
import { AbstractCookieService } from './abstract.cookie.service';
import { Constants } from 'src/app/app.constants';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { selectTheme } from 'src/app/store/selectors';
import { ActionTypes } from 'src/app/store/action.types';

@Injectable()
export class ThemeService extends AbstractCookieService {
  constructor(private _store: Store) {
    super();
  }

  getCurrentTheme(): Observable<string> {
    return this._store.select(selectTheme);
  }

  setTheme(theme: string): void {
    this.setCookie(Constants.themeCookie, theme);
    this._store.dispatch(ActionTypes.setTheme(theme));
  }
}
