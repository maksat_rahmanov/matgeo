import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AbstractCookieService } from './abstract.cookie.service';
import { Constants } from 'src/app/app.constants';

@Injectable()
export class LanguageService extends AbstractCookieService {
  constructor(private _router: Router) {
    super();
  }

  setLanguage(language: string): void {
    this.setCookie(Constants.languageCookie, language);
    window.open(this._router.url, '_self');
  }
}
