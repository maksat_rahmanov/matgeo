import { createFeatureSelector, createSelector } from "@ngrx/store";
import { CountriesState, selectAll } from "./reducers";
import { AppState } from "./index";

const selectCountriesState = createFeatureSelector<CountriesState>("countries");
const selectAppState = createFeatureSelector<AppState>("app");

export const selectAllCountries = createSelector(
  selectCountriesState,
  selectAll
);

export const selectCountry = createSelector(
  selectAppState,
  (state) => state.selectedCountry
);

export const selectError = createSelector(
  selectAppState,
  (state) => state.isError
);

export const selectLoading = createSelector(
  selectAppState,
  (state) => state.isLoading
);

export const selectTheme = createSelector(
  selectAppState,
  (state) => state.theme
);

export const selectAppTitle = createSelector(
  selectAppState,
  (state) => state.title
);

export const selectPageInfo = createSelector(
  selectAppState,
  (state) => state.pageInfo
);

export const selectCountryName = (id: string) =>
  createSelector(selectAllCountries, (countries) => {
    const country = countries.find((country) => country.id === id);
    return country?.name || "";
  });

//TODO: remove selector
export const selectBySearchString = (searchString: string) =>
  createSelector(selectAllCountries, (countries) => {
    const filtered = countries.filter(
      (country) =>
        country.name.toLowerCase().includes(searchString) ||
        country.capital.toLowerCase().includes(searchString)
    );

    return filtered.length ? filtered : countries;
  });
