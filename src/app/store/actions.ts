import { createAction, props } from "@ngrx/store";
import { Country } from "../model/country";

export const loadCountries = createAction("[AppModule] Load countries");

export const loadNextCountries = createAction(
  "[Countries list component] Load next countries"
);

export const loadCountryById = createAction(
  "[AppModule] Load country by id",
  (id: string) => ({ id })
);

export const countriesLoaded = createAction(
  "[AppModule] Countries loaded",
  props<{ countries: Country[] }>()
);

export const searchedCountriesLoaded = createAction(
  "[Home component] Searched countries loaded",
  props<{ countries: Country[] }>()
);

export const countryLoaded = createAction(
  "[AppModule] Country loaded",
  (country: Country | undefined) => ({ country })
);

export const setTheme = createAction(
  "[Switch language component] Set app theme",
  (theme: string) => ({ theme })
);

export const setAppTitle = createAction(
  "[AppModule] Set app title",
  (title: string) => ({ title })
);

export const setErrorState = createAction(
  "[AppModule] Set error state",
  props<{ isError: boolean }>()
);

export const setTitleByCountry = createAction(
  "[Detail component] Set app title by country name",
  (id: string) => ({ id })
);

export const setCountryId = createAction(
  "[App] Set selected country id",
  (id: string) => ({ id })
);

export const setLoading = createAction(
  "[App] Set loading state",
  (isLoading: boolean) => ({ isLoading })
);

export const setPaginationInfo = createAction(
  "[App] Set pagination info",
  (pageInfo: PageInfo) => ({ pageInfo })
);

export const searchCountry = createAction(
  "[Home component] Search country",
  (searchString: string) => ({ searchString })
);
