import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { CountriesApiService } from "../graphcms/api/countries.api";
import { ActionTypes } from "./action.types";
import {
  catchError,
  concatMap,
  map,
  switchMap,
  tap,
  withLatestFrom
} from "rxjs/operators";
import { filter, of } from "rxjs";
import { Store } from "@ngrx/store";
import { selectCountryName, selectPageInfo } from "./selectors";

@Injectable()
export class StoreEffects {
  countries$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ActionTypes.loadCountries),
      switchMap(() =>
        this.countriesService.getCountries().pipe(
          catchError(() => {
            this.store.dispatch(ActionTypes.setErrorState({ isError: true }));
            return of(undefined);
          }),
          switchMap((countriesConnection) =>
            of(
              ActionTypes.countriesLoaded({
                countries: countriesConnection?.countries || []
              }),
              ActionTypes.setPaginationInfo(
                countriesConnection?.pageInfo || {
                  endCursor: "",
                  hasNextPage: true
                }
              )
            )
          )
        )
      )
    )
  );

  country$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ActionTypes.loadCountryById),
      tap(() => this.store.dispatch(ActionTypes.setLoading(true))),
      switchMap((action) =>
        this.countriesService.getCountryById(action.id).pipe(
          catchError(() => {
            this.store.dispatch(ActionTypes.setErrorState({ isError: true }));
            return of(undefined);
          }),
          switchMap((country) =>
            of(
              ActionTypes.setCountryId(action.id),
              ActionTypes.countryLoaded(country),
              ActionTypes.setLoading(false)
            )
          )
        )
      )
    )
  );

  setCountryName$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ActionTypes.setTitleByCountry),
      concatMap((action) =>
        of(action.id).pipe(
          withLatestFrom(this.store.select(selectCountryName(action.id))),
          map(([, name]) => name)
        )
      ),
      map((name: string) => ActionTypes.setAppTitle(name))
    )
  );

  pagination$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ActionTypes.loadNextCountries),
      withLatestFrom(this.store.select(selectPageInfo)),
      map(([, pageInfo]) => pageInfo),
      filter((pageInfo) => !!pageInfo.hasNextPage),
      switchMap((pageInfo) =>
        this.countriesService.getNextCountries(pageInfo.endCursor)
      ),
      switchMap((countriesConnection) =>
        of(
          ActionTypes.countriesLoaded({
            countries: countriesConnection?.countries || []
          }),
          ActionTypes.setPaginationInfo(countriesConnection?.pageInfo)
        )
      )
    )
  );

  searchCountry$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ActionTypes.searchCountry),
      tap(() => this.store.dispatch(ActionTypes.setLoading(true))),
      switchMap((action) =>
        this.countriesService
          .getCountriesBySearchString(action.searchString)
          .pipe(
            switchMap((countriesConnection) =>
              of(
                ActionTypes.searchedCountriesLoaded({
                  countries: countriesConnection?.countries || []
                }),
                ActionTypes.setPaginationInfo(countriesConnection?.pageInfo),
                ActionTypes.setLoading(false),
                ActionTypes.countryLoaded(undefined)
              )
            )
          )
      )
    )
  );

  constructor(
    private actions$: Actions,
    private countriesService: CountriesApiService,
    private store: Store
  ) {}
}
