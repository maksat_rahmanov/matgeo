import { Country } from "../model/country";

export interface AppState {
  theme: string;
  title: string;
  isError: boolean;
  isLoading: boolean;
  selectedCountryId: string;
  selectedCountry: Country | undefined;
  pageInfo: PageInfo;
}

export const appInitialState: AppState = {
  theme: "",
  title: "",
  isError: false,
  isLoading: false,
  selectedCountryId: "",
  selectedCountry: undefined,
  pageInfo: {
    endCursor: "",
    hasNextPage: true
  }
};
