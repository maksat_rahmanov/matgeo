import { createEntityAdapter, EntityState } from "@ngrx/entity";
import { Country } from "../model/country";
import { createReducer, on } from "@ngrx/store";
import { ActionTypes } from "./action.types";
import { appInitialState } from "./index";

export interface CountriesState extends EntityState<Country> {}

export const adapter = createEntityAdapter<Country>({
  sortComparer: (country1: Country, country2: Country) =>
    country1.name.localeCompare(country2.name)
});

const countriesInitialState = adapter.getInitialState();

export const countriesReducer = createReducer(
  countriesInitialState,
  on(ActionTypes.countriesLoaded, (state, action) =>
    adapter.addMany(action.countries, state)
  ),
  on(ActionTypes.searchedCountriesLoaded, (state, action) =>
    adapter.setAll(action.countries, state)
  )
);

export const appReducer = createReducer(
  appInitialState,
  on(ActionTypes.setErrorState, (state, action) => {
    return {
      ...state,
      isError: action.isError
    };
  }),
  on(ActionTypes.setTheme, (state, action) => {
    return {
      ...state,
      theme: action.theme
    };
  }),
  on(ActionTypes.setAppTitle, (state, action) => {
    return {
      ...state,
      title: action.title
    };
  }),
  on(ActionTypes.setCountryId, (state, action) => {
    return {
      ...state,
      selectedCountryId: action.id
    };
  }),
  on(ActionTypes.countryLoaded, (state, action) => {
    return {
      ...state,
      selectedCountry: action.country
    };
  }),
  on(ActionTypes.setLoading, (state, action) => {
    return {
      ...state,
      isLoading: action.isLoading
    };
  }),
  on(ActionTypes.setPaginationInfo, (state, action) => {
    return {
      ...state,
      pageInfo: action.pageInfo
    };
  })
);

export const { selectAll } = adapter.getSelectors();
