export interface BorderCountryDto {
  id: string;
  name: string;
  flag: string;
}
