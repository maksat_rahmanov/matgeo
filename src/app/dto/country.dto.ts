import { BorderCountry } from '../model/border-country';

export interface CountryDto {
  id: string;
  name: string;
  capital: string;
  region?: string;
  subregion?: string;
  area?: number;
  population?: number;
  flag?: string;
  heroImage?: string;
  latitude?: number;
  longitude?: number;
  alpha2Code?: string;
  alpha3Code?: string;
  topLevelDomain?: string;
  callingCodes?: string[];
  timeZones?: string[];
  currencies?: string[];
  languages?: string[];
  borderCountries?: BorderCountry[];
}
