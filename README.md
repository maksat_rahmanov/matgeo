# Material Geography

This project was originally generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.1.4 with later update to version 16.

## Live preview

[Material Geography](https://matgeo.vercel.app/)

## Figma layout

[Project layout](https://www.figma.com/file/SUuSCUii3AhPLkjGqorWvz/Material-Geography?type=design&node-id=0%3A1&mode=design&t=dcqsAWXbCCpgjtgS-1)

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.
